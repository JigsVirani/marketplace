//
//  RegisterCourierServiceVC.swift
//  MarketPlace
//
//  Created by Sandeep on 18/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class RegisterCourierServiceVC: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var imgLogo: UIImageView!
    @IBOutlet var imgCheckBox: UIImageView!
    
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        
        imgLogo.layer.cornerRadius = 8
        btnEdit.layer.cornerRadius = 14
        btnDelete.layer.cornerRadius = 14
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnLogoAction(_ sender: UIButton) {
        if sender == btnEdit {
            let alert = UIAlertController(title: nil, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alert.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                    let imag = UIImagePickerController()
                    imag.delegate = self
                    imag.sourceType = .camera
                    self.present(imag, animated: true, completion: nil)
                }
            })
            alert.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                    let imag = UIImagePickerController()
                    imag.delegate = self
                    imag.sourceType = .photoLibrary
                    self.present(imag, animated: true, completion: nil)
                }
            })
            alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
            })
            self.present(alert, animated: true, completion: nil)
        }else{
            imgLogo.image = nil
        }
    }
    
    @IBAction func btnTermsConditionAction(_ sender: UIButton) {
        if sender.isSelected == true {
            sender.isSelected = false
            imgCheckBox.image = UIImage(named: "check_box")
        }else{
            sender.isSelected = true
            imgCheckBox.image = UIImage(named: "check_box_fiiled")
        }
    }
    
    //MARK: ImagePicker Delegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imgLogo.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
