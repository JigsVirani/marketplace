//
//  StoreOrdersVC.swift
//  MarketPlace
//
//  Created by Sandeep on 08/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class StoreOrdersVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var btnActive: UIButton!
    @IBOutlet var btnPastOrders: UIButton!
    @IBOutlet var tblProductList: UITableView!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    
    func initialization() {
        btnActive.layer.cornerRadius = 15
        btnPastOrders.layer.cornerRadius = 15
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnTabsAction(_ sender: UIButton) {
        
        btnActive.backgroundColor = UIColor.white
        btnActive.setTitleColor(UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0), for: UIControlState.normal)
        btnPastOrders.backgroundColor = UIColor.white
        btnPastOrders.setTitleColor(UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0), for: UIControlState.normal)
        
        if sender.tag == 1 {
            btnActive.backgroundColor = UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)
            btnActive.setTitleColor(UIColor.white, for: UIControlState.normal)
        }else if sender.tag == 2{
            btnPastOrders.backgroundColor = UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)
            btnPastOrders.setTitleColor(UIColor.white, for: UIControlState.normal)
        }
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:StoreOrdersTableCell = tableView.dequeueReusableCell(withIdentifier: "StoreOrdersTableCell", for: indexPath as IndexPath) as! StoreOrdersTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.viewBG.layer.cornerRadius = 8
        
        cell.btnCompleteOrder.layer.cornerRadius = 12
        cell.btnCancelOrder.layer.cornerRadius = 12
        
        return cell;
    }
}

class StoreOrdersTableCell: UITableViewCell {
    
    @IBOutlet var viewBG: UIView!
    
    @IBOutlet var btnCompleteOrder: UIButton!
    @IBOutlet var btnCancelOrder: UIButton!
}
