
//
//  StoresOrderDetailVC.swift
//  MarketPlace
//
//  Created by Sandeep on 11/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class MyStoreVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var colProducts: UICollectionView!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    
    func initialization() {
        
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Product Name", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)])
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    //MARK:- CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell : StoresOrderDetailCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresOrderDetailCollectionCell", for: indexPath) as! StoresOrderDetailCollectionCell
        
        cell.viewBG.layer.cornerRadius = 8
        cell.btnEdit.layer.cornerRadius = 11
        cell.btnDelete.layer.cornerRadius = 11
        
        return cell
    }
    
    func collectionView(_ collectionView : UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
    
        return CGSize.init(width: ((Constants.ScreenSize.SCREEN_WIDTH-32)/2)-5, height: 244)
    }
}

class StoresOrderDetailCollectionCell: UICollectionViewCell {
    @IBOutlet var viewBG: UIView!
    
    @IBOutlet var btnEdit: UIButton!
    @IBOutlet var btnDelete: UIButton!
}
