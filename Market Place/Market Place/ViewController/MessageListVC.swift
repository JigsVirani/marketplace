//
//  MessageListVC.swift
//  MarketPlace
//
//  Created by Sandeep on 12/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class MessageListVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var txtSearch: UITextField!
    @IBOutlet var tblMessageList: UITableView!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    
    func initialization() {
        
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Name", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)])
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:MessageListTableCell = tableView.dequeueReusableCell(withIdentifier: "MessageListTableCell", for: indexPath as IndexPath) as! MessageListTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.viewBG.layer.cornerRadius = 5
        cell.imgCountBG.layer.cornerRadius = 10
        
        return cell;
    }
}

class MessageListTableCell: UITableViewCell {
    @IBOutlet var viewBG: UIView!
    
    @IBOutlet var imgCountBG: UIImageView!
}
