//
//  AddNewProductVC.swift
//  MarketPlace
//
//  Created by Sandeep on 12/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class AddNewProductVC: UIViewController, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var imgName: UIImageView!
    @IBOutlet var imgPrice: UIImageView!
    @IBOutlet var imgCategories: UIImageView!
    
    @IBOutlet var colImages: UICollectionView!
    @IBOutlet var constCollectionImagesHeight: NSLayoutConstraint!
    
    @IBOutlet var btnColor: UIButton!
    @IBOutlet var btnSize: UIButton!
    @IBOutlet var btnPrice: UIButton!
    @IBOutlet var btnSave: UIButton!
    
    @IBOutlet var txtViewDescription: UITextView!
    
    //MARK: Other Objects
    var arrProductImages = NSMutableArray()
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        imgName.layer.cornerRadius = 20
        imgPrice.layer.cornerRadius = 20
        imgCategories.layer.cornerRadius = 20
        
        btnColor.layer.cornerRadius = 20
        btnSize.layer.cornerRadius = 20
        btnPrice.layer.cornerRadius = 20
        btnSave.layer.cornerRadius = 20
        
        txtViewDescription.layer.cornerRadius = 8
        
        arrProductImages.add(UIImage(named: "plush_btn_new_product")!)
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrProductImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell : AddNewProductImagesCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddNewProductImagesCollectionCell", for: indexPath) as! AddNewProductImagesCollectionCell
        
        cell.imgProduct.layer.cornerRadius = 8
        
        cell.imgProduct.image = arrProductImages.object(at: indexPath.row) as? UIImage
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath) {
        
        if indexPath.row == arrProductImages.count - 1 {
            
            let alert = UIAlertController(title: nil, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alert.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                    let imag = UIImagePickerController()
                    imag.delegate = self
                    imag.sourceType = .camera
                    self.present(imag, animated: true, completion: nil)
                }
            })
            alert.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                    let imag = UIImagePickerController()
                    imag.delegate = self
                    imag.sourceType = .photoLibrary
                    self.present(imag, animated: true, completion: nil)
                }
            })
            alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
            })
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK: ImagePicker Delegate
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            arrProductImages.insert(pickedImage, at: arrProductImages.count-1)
        }
        picker.dismiss(animated: true, completion: nil)
        
        var roundedup = arrProductImages.count / 2
        if arrProductImages.count % 2 != 0 {
            roundedup = Int(ceil(Double(CGFloat(arrProductImages.count) / 2.0)))
        }
        constCollectionImagesHeight.constant = CGFloat(roundedup*130)+CGFloat((roundedup-1)*25)
        
        colImages.reloadData()
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

class AddNewProductImagesCollectionCell: UICollectionViewCell {
    
    @IBOutlet var imgProduct: UIImageView!
}
