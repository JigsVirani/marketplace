//
//  MyProfileVC.swift
//  MarketPlace
//
//  Created by Sandeep on 12/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class MyProfileVC: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var imgProfilePic: UIImageView!
    
    @IBOutlet var txtFirstName: UITextField!
    @IBOutlet var txtLastName: UITextField!
    
    @IBOutlet var imgFirstName: UIImageView!
    @IBOutlet var imgLastName: UIImageView!
    
    @IBOutlet var btnUpdate: UIButton!
    
    //MARK: ChangePasswordView
    @IBOutlet var viewChangePassword: UIView!
    
    @IBOutlet var txtCurrentPassword: UITextField!
    @IBOutlet var txtNewPassword: UITextField!
    
    @IBOutlet var imgCurrentPassword: UIImageView!
    @IBOutlet var imgNewPassword: UIImageView!
    @IBOutlet var imgChangePasswordBG: UIImageView!
    
    @IBOutlet var btnChangeMyPassword: UIButton!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    
    func initialization() {
        
        txtFirstName.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [NSForegroundColorAttributeName: UIColor.white])
        txtLastName.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        imgFirstName.layer.cornerRadius = 20
        imgLastName.layer.cornerRadius = 20
        btnUpdate.layer.cornerRadius = 20
        
        imgCurrentPassword.layer.cornerRadius = 20
        imgNewPassword.layer.cornerRadius = 20
        imgChangePasswordBG.layer.cornerRadius = 8
        imgProfilePic.layer.cornerRadius = 53
        btnChangeMyPassword.layer.cornerRadius = 20
        
        viewChangePassword.frame = CGRect(x: 0, y: 0, width: Constants.ScreenSize.SCREEN_WIDTH, height: Constants.ScreenSize.SCREEN_HEIGHT)
        self.view.addSubview(viewChangePassword)
        viewChangePassword.isHidden = true
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnProfilePicAction(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .camera
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .photoLibrary
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnChangePasswordAction(_ sender: UIButton) {
        viewChangePassword.isHidden = false
    }
    
    //MARK: ChangePasswordView
    @IBAction func btnClostChangePassowrdViewAction(_ sender: UIButton) {
        
        viewChangePassword.isHidden = true
    }
    
    //MARK:- ImagePicker
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgProfilePic.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}
