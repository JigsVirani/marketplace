//
//  RightMenuCell.swift
//  PTO
//
//  Created by fiplmac1 on 23/08/16.
//  Copyright © 2016 fusion. All rights reserved.
//

import UIKit

class LeftMenuCell: UITableViewCell
{
    @IBOutlet var lblMenuTitle: UILabel!
    @IBOutlet var imgMenuIcon: UIImageView!
    @IBOutlet var imgBG: UIImageView!
    
    override func awakeFromNib()
    {
        imgBG.layer.cornerRadius = 8
        
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
    }
    
}
