//
//  LeftMenuVC.swift
//  yump
//
//  Created by Fusion on 16/07/16.
//  Copyright © 2016 Fusion. All rights reserved.
//

import UIKit
import Social

class LeftMenuVC: UIViewController
{
    @IBOutlet var btnProfilePic: UIButton!
    @IBOutlet var btnUserType: UIButton!
    
    @IBOutlet var lblUserType: UILabel!
    
    @IBOutlet var tblMenuList: UITableView!
    
    var cellIdentifier = NSString()
    var arrMenuTitle = NSMutableArray()
    var arrMenuImages = NSMutableArray()

    //MARK:- View Life Cycle
    override func viewDidLoad(){
        super.viewDidLoad()
                
        NotificationCenter.default.addObserver(self, selector: #selector(LeftMenuVC.reloadUserData), name: NSNotification.Name(rawValue: "reloadUserData"), object: nil)
        
        tblMenuList.register(UINib.init(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: "LeftMenuCell")
        
        self.reloadUserData()
        
        self.btnProfilePic.layer.cornerRadius = 8
        self.btnUserType.layer.cornerRadius = 8
    }
    
    func reloadUserData(){
        
        if Constants.appDelegate.intUserType == 1 {
            arrMenuTitle = ["Contact Us", "Messages", "Settings", "Become Courier Service Provider", "Login"]
            arrMenuImages = ["Contact_Us_icon", "Messages_icon", "Settings_icon", "Become_Courier_icon", "Logout_icon"]
            
            lblUserType.text = "Become Vendor"
            btnUserType.setImage(UIImage.init(named: "Become_Vendor_icon"), for: UIControlState.normal)
            
        }else if Constants.appDelegate.intUserType == 2{
            arrMenuTitle = ["My Store", "Messages", "Delivery Agencies", "Settings", "Become Courier Service Provider", "Store Orders", "Login"]
            arrMenuImages = ["My_Store_icon", "Messages_icon", "Delivery_Agencies_icon", "Settings_icon", "Become_Courier_icon", "", "Logout_icon"]
            
            lblUserType.text = "Switch to User"
            btnUserType.setImage(UIImage.init(named: "Switch_to_User_icon"), for: UIControlState.normal)
        }
        
        if ProjectSharedObj.sharedInstance.isLogedIn == true {
            let index = arrMenuTitle.index(of: "Login")
            if index != NSNotFound {
                arrMenuTitle.replaceObject(at: index, with: "Logout")
            }
        }
        
        tblMenuList.reloadData()
    }

    override func didReceiveMemoryWarning(){
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews(){
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMyProfileAction(_ sender: UIButton) {
        let parentViewController = SlideNavigationController.sharedInstance().viewControllers.last!
        let className = NSStringFromClass(parentViewController.classForCoder)
        if className == "MarketPlace.MyProfileVC"{
            SlideNavigationController.sharedInstance().closeMenu(completion: nil)
        }else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }
    }
    
    @IBAction func btnUserTypeAction(_ sender: UIButton) {
        
        if Constants.appDelegate.intUserType == 1 {
            
            Constants.appDelegate.intUserType = 2
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "StoreOrdersVC") as! StoreOrdersVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }else{
            Constants.appDelegate.intUserType = 1
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = (storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC)
            SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
        }
        
        self.reloadUserData()
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrMenuTitle.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:LeftMenuCell = tableView.dequeueReusableCell(withIdentifier: "LeftMenuCell", for: indexPath as IndexPath) as! LeftMenuCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.lblMenuTitle.text = arrMenuTitle.object(at: indexPath.row) as? String
        cell.imgMenuIcon.image = UIImage.init(named: arrMenuImages.object(at: indexPath.row) as! String)
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        
        let parentViewController = SlideNavigationController.sharedInstance().viewControllers.last!
        let className = NSStringFromClass(parentViewController.classForCoder)
        
        if arrMenuTitle.object(at: indexPath.row) as! String == "Delivery Agencies" {
            if className == "MarketPlace.StoresListingVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "StoresListingVC") as! StoresListingVC)
                controller.isDeliveryAgencies = true
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Store Orders" {
            if className == "MarketPlace.StoreOrdersVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "StoreOrdersVC") as! StoreOrdersVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Messages" {
            if className == "MarketPlace.MessageListVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "MessageListVC") as! MessageListVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Login" {
            if className == "MarketPlace.LoginVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Logout" {
            ProjectSharedObj.sharedInstance.isLogedIn = false
            self.reloadUserData()
            if className == "MarketPlace.DashboardVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "My Store" {
            if className == "MarketPlace.MyStoreVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "MyStoreVC") as! MyStoreVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }else if arrMenuTitle.object(at: indexPath.row) as! String == "Become Courier Service Provider" {
            if className == "MarketPlace.RegisterCourierServiceVC"{
                SlideNavigationController.sharedInstance().closeMenu(completion: nil)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = (storyboard.instantiateViewController(withIdentifier: "RegisterCourierServiceVC") as! RegisterCourierServiceVC)
                SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
            }
        }
    }
}
