//
//  ViewController.swift
//  Market Place
//
//  Created by Sandeep on 04/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit
import MBProgressHUD

class DashboardVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var lblCartCount: UILabel!
    @IBOutlet var imgCartCount: UIImageView!
    @IBOutlet var txtSearch: UITextField!
    
    @IBOutlet var tblProductList: UITableView!
    
    //MARK: Other Objects
    var arrCategoryList = NSMutableArray()
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Category Name", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)])
        
        imgCartCount.layer.cornerRadius = 9
        
        self.getAllCategoryJSon()
    }
    
    //MARK:- Web Service Call
    func getAllCategoryJSon() {
        let strURL = "\(ProjectSharedObj.sharedInstance.baseUrl)/getallcategories"
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        API.getResponseFromApi(strUrl: strURL, completionHandler: { (dictResponse) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if dictResponse.value(forKey: "status") != nil {
                
                if dictResponse.value(forKey: "status") as! Bool == true {
                    
                    self.arrCategoryList.addObjects(from: (dictResponse.value(forKey: "data") as! NSArray) as! [Any])
                    
                    self.tblProductList.reloadData()
                    
                    return
                }
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        })
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return arrCategoryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:DashboardTableCell = tableView.dequeueReusableCell(withIdentifier: "DashboardTableCell", for: indexPath as IndexPath) as! DashboardTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell;
    }
}

class DashboardTableCell: UITableViewCell {
    
}

