//
//  MyCartVC.swift
//  Market Place
//
//  Created by Sandeep on 05/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class MyCartVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var tblProductList: UITableView!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell{
        
        let cell:MyCartTableCell = tableView.dequeueReusableCell(withIdentifier: "MyCartTableCell", for: indexPath as IndexPath) as! MyCartTableCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.viewBG.layer.cornerRadius = 8
        
        cell.btnRemove.layer.cornerRadius = 12
        cell.btnPlaceOrder.layer.cornerRadius = 12
        
        cell.colSubProducts.dataSource = self
        cell.colSubProducts.delegate = self
        
        return cell;
    }
    
    //MARK:- CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        let cell : MyCartCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCartCollectionCell", for: indexPath) as! MyCartCollectionCell
        
        return cell
    }
}

class MyCartTableCell: UITableViewCell {
    @IBOutlet var viewBG: UIView!
    
    @IBOutlet var colSubProducts: UICollectionView!
    
    @IBOutlet var btnRemove: UIButton!
    @IBOutlet var btnPlaceOrder: UIButton!
}

class MyCartCollectionCell: UICollectionViewCell {
    
}
