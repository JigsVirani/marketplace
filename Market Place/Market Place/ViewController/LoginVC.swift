//
//  LoginVC.swift
//  MarketPlace
//
//  Created by Sandeep on 12/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit
import MBProgressHUD
import Alamofire

class LoginVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var imgUserName: UIImageView!
    @IBOutlet var imgPassword: UIImageView!
    
    @IBOutlet var btnLogin: UIButton!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        
        txtUserName.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [NSForegroundColorAttributeName: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        imgUserName.layer.cornerRadius = 20
        imgPassword.layer.cornerRadius = 20
        btnLogin.layer.cornerRadius = 20
    }
    
    //MARK:- Web Service Call
    func loginJSon() {
        
        let strURL = "\(ProjectSharedObj.sharedInstance.baseUrl)/dologin"
        
        let dict: Parameters = ["email_address": txtUserName.text!, "password": txtPassword.text!]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        API.getResponseFromApi(strUrl: strURL, param: dict as? Dictionary<String, String>) { (dictResponse) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            if dictResponse.value(forKey: "status") != nil {
                
                if dictResponse.value(forKey: "status") as! String == "1" {
                    ProjectSharedObj.sharedInstance.isLogedIn = true
                   
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserData"), object: nil)
                    
                    for controller: Any in (self.navigationController?.viewControllers)! {
                        if (controller is DashboardVC) {
                            self.navigationController?.popToViewController(controller as! DashboardVC, animated: true)
                            return
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC)
                    SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
                    
                    return
                }
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnMenuAction(_ sender: UIButton) {
        SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
    }
    
    @IBAction func btnTakeMeInAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtUserName.text == ""{
            ProjectUtility.displayTost(erroemessage: "Please enter user name")
        }else if txtPassword.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter password")
        }else{
            self.loginJSon()
        }
    }
}
