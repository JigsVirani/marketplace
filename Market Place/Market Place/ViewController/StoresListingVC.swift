//
//  StoresListingVC.swift
//  Market Place
//
//  Created by Sandeep on 04/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class StoresListingVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var btnBack: UIButton!
    
    @IBOutlet var lblScreenTitle: UILabel!
    @IBOutlet var lblCartCount: UILabel!
    
    @IBOutlet var imgCartCount: UIImageView!
    @IBOutlet var txtSearch: UITextField!
    
    @IBOutlet var colStoresList: UICollectionView!
    
    @IBOutlet var constCartButtonWidth: NSLayoutConstraint!
    
    //MARK: Other Objects
    var isDeliveryAgencies = false
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search by Name", attributes: [NSForegroundColorAttributeName: UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)])
        
        imgCartCount.layer.cornerRadius = 9
        
        if isDeliveryAgencies {
            lblScreenTitle.text = "Delivery Agencies"
            constCartButtonWidth.constant = 0
            btnBack.setImage(UIImage.init(named: "menu_icon"), for: UIControlState.normal)
        }
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnBackAction(_ sender: UIButton) {
        
        if !isDeliveryAgencies {
            _ = self.navigationController?.popViewController(animated: true)
        }else{
            SlideNavigationController.sharedInstance().open(MenuLeft, withCompletion: nil)
        }
    }
    
    //MARK:- CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        let cell : StoreListingCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreListingCollectionCell", for: indexPath) as! StoreListingCollectionCell
        
        cell.viewBG.layer.cornerRadius = 8
        cell.imgBG.layer.cornerRadius = 8
        cell.btn.layer.cornerRadius = 12.5
        
        cell.imgBG.layer.shadowRadius = 6
        cell.imgBG.layer.shadowOffset = CGSize.init(width: 0, height: 8)
        cell.imgBG.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.imgBG.layer.shadowOpacity = 0.75
        
        cell.btn.layer.shadowRadius = 3
        cell.btn.layer.shadowOffset = CGSize.init(width: 0, height: 3)
        cell.btn.layer.shadowColor = UIColor.init(red: 39.0/255.0, green: 46.0/255.0, blue: 54.0/255.0, alpha: 1.0).cgColor
        cell.btn.layer.shadowOpacity = 0.75
        
        return cell
    }
    
    func collectionView(_ collectionView : UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
        
        return CGSize.init(width: ((Constants.ScreenSize.SCREEN_WIDTH-14)/2)-3.5, height: 240)
    }
}

class StoreListingCollectionCell: UICollectionViewCell {
    @IBOutlet var viewBG: UIView!
    @IBOutlet var imgBG: UIView!
    
    @IBOutlet var btn: UIButton!
}
