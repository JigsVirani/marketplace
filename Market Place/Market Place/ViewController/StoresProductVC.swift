//
//  StoresProductVC.swift
//  Market Place
//
//  Created by Sandeep on 05/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit

class StoresProductVC: UIViewController {

    //MARK:- Outlet Declaration
    @IBOutlet var lblCartCount: UILabel!
    @IBOutlet var imgCartCount: UIImageView!
    
    @IBOutlet var colProductTabs: UICollectionView!
    @IBOutlet var colProductList: UICollectionView!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        imgCartCount.layer.cornerRadius = 9
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- CollectionView Delegate
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAtIndexPath indexPath: IndexPath) -> UICollectionViewCell{
        
        if collectionView == colProductTabs {
            let cell : StoresProductTabsCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresProductTabsCollectionCell", for: indexPath) as! StoresProductTabsCollectionCell
            
            cell.imgBG.layer.cornerRadius = 15
            
            if indexPath.row == 0 {
                cell.imgBG.backgroundColor = UIColor.init(red: 19.0/255.0, green: 132.0/255.0, blue: 104.0/255.0, alpha: 1.0)
                cell.lblTitle.textColor = UIColor.white
            }else {
                cell.imgBG.backgroundColor = UIColor.white
                cell.lblTitle.textColor = UIColor.black
            }
            
            return cell
        }else if collectionView == colProductList {
            let cell : StoresProductListCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoresProductListCollectionCell", for: indexPath) as! StoresProductListCollectionCell
            
            cell.viewBG.layer.cornerRadius = 8
            
            return cell
        }
        let cell : UICollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UICollectionViewCell", for: indexPath) 
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: IndexPath){
        
    }

    func collectionView(_ collectionView : UICollectionView, layout collectionViewLayout:UICollectionViewLayout, sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize{
        
        if collectionView == colProductTabs {
            return CGSize.init(width: 151, height: 30)
        }else if collectionView == colProductList {
            return CGSize.init(width: ((Constants.ScreenSize.SCREEN_WIDTH-32)/2)-5, height: 244)
        }
        return CGSize.init(width: 0, height: 0)
    }
}

class StoresProductTabsCollectionCell: UICollectionViewCell {
    @IBOutlet var imgBG: UIImageView!
    @IBOutlet var lblTitle: UILabel!
}

class StoresProductListCollectionCell: UICollectionViewCell {
    @IBOutlet var viewBG: UIView!
}
