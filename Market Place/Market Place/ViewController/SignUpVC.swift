//
//  SignUpVC.swift
//  MarketPlace
//
//  Created by Sandeep on 12/09/17.
//  Copyright © 2017 Sandeep. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import MBProgressHUD
import Alamofire

class SignUpVC: UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    //MARK:- Outlet Declaration
    @IBOutlet var txtUserName: UITextField!
    @IBOutlet var txtMobileNumber: UITextField!
    @IBOutlet var txtPassword: UITextField!
    
    @IBOutlet var imgProfilePic: UIImageView!
    
    @IBOutlet var imgUserName: UIImageView!
    @IBOutlet var imgMobileNumber: UIImageView!
    @IBOutlet var imgPassword: UIImageView!
    
    @IBOutlet var btnSignUp: UIButton!
    
    //MARK: Other Objects
    
    //MARK:- View Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Initialization
    func initialization() {
        
        txtUserName.attributedPlaceholder = NSAttributedString(string: "User Name", attributes: [NSForegroundColorAttributeName: UIColor.white])
        txtMobileNumber.attributedPlaceholder = NSAttributedString(string: "Mobile Number", attributes: [NSForegroundColorAttributeName: UIColor.white])
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSForegroundColorAttributeName: UIColor.white])
        
        imgProfilePic.layer.cornerRadius = 45
        imgUserName.layer.cornerRadius = 20
        imgMobileNumber.layer.cornerRadius = 20
        imgPassword.layer.cornerRadius = 20
        btnSignUp.layer.cornerRadius = 20
    }
    
    //MARK:- Web Service Call
    func registerJSon() {
        
        let strURL = "\(ProjectSharedObj.sharedInstance.baseUrl)/doregister"
        
        let dict: Parameters = ["email_address": txtUserName.text!, "password": txtPassword.text!, "phone": txtMobileNumber.text!]
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        API.getResponseFromApi(strUrl: strURL, param: dict as? Dictionary<String, String>) { (dictResponse) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if dictResponse.value(forKey: "status") != nil {
                
                if dictResponse.value(forKey: "status") as! Bool == true {
                    
                    ProjectSharedObj.sharedInstance.isLogedIn = true
                    Constants.appDelegate.dictUserDetail = (dictResponse.value(forKey: "data") as! NSDictionary).mutableCopy() as! NSMutableDictionary
                    
                    Constants.appDelegate.dictUserDetail.setValue(self.txtPassword.text, forKey: "password")
                    
                    let archivedUser = NSKeyedArchiver.archivedData(withRootObject: Constants.appDelegate.dictUserDetail)
                    UserDefaults.standard.setValue(archivedUser, forKey: "UserDetail")
                    UserDefaults.standard.synchronize()
                    
                    for controller: Any in (self.navigationController?.viewControllers)! {
                        if (controller is DashboardVC) {
                            self.navigationController?.popToViewController(controller as! DashboardVC, animated: true)
                            return
                        }
                    }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller = (storyboard.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC)
                    SlideNavigationController.sharedInstance().pushViewController(controller, animated: false)
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadUserData"), object: nil)
                    
                    return
                }
                ProjectUtility.displayTost(erroemessage: dictResponse.value(forKey: "message") as! String)
                return
            }
            ProjectUtility.displayTost(erroemessage: "We are having some problem")
        }
    }
    
    //MARK:- Button TouchUp
    @IBAction func btnBackAction(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnProfilePicAction(_ sender: UIButton) {
        let alert = UIAlertController(title: nil, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alert.addAction(UIAlertAction.init(title: "Take Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .camera
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Choose Photo", style: UIAlertActionStyle.default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary){
                let imag = UIImagePickerController()
                imag.delegate = self
                imag.sourceType = .photoLibrary
                self.present(imag, animated: true, completion: nil)
            }
        })
        alert.addAction(UIAlertAction.init(title: "Cancel", style: UIAlertActionStyle.cancel) { (action) in
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnSignUpPicAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if txtUserName.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter user name")
        }else if (txtMobileNumber.text?.characters.count)! < 10 {
            ProjectUtility.displayTost(erroemessage: "Please enter valid mobile number")
        }else if txtPassword.text == "" {
            ProjectUtility.displayTost(erroemessage: "Please enter password")
        }else{
            self.registerJSon()
        }
    }
    
    //MARK:- ImagePicker
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]){
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imgProfilePic.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}
